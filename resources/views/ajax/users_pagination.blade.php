
<table id="datatables" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
    <thead class="text-primary">
      <tr><th class="text-center">
        #
      </th>
      <th></th>
      <th>
        Name
      </th>
      <th>
        Email
      </th>
      <th>
        Phone
      </th>
      <th>
        Address
      </th>
      <th>
        Membership Code
      </th>
      <th>
        Membership Type
      </th>
      <th>
        Status
      </th>
      <th>
        Date
      </th>
     
    </tr>
   </thead>
   <tbody>
    
   @if(count($data) > 0)
    @foreach ($data as $key => $row)
    {{-- {{dd($paymentPlans)}} --}}
    <tr>
      <td class="text-center">
       {{$key+=1}}
      </td>
      <td>
          <div class="">
              <div class="img-wrapper">
                @if (!isset($row->memberFormimg))
                <img src="{{asset('assets/img/image_placeholder.jpg')}}">
                @else
                <img src=" {{asset('/storage/'.$row->memberFormimg) }}">
                @endif  
              </div>
          </div>
      </td>
      <td>
        {{$row->fname}} {{$row->mname}} {{$row->lname}}
      </td>
      <td>
        {{$row->email}}
      </td>
      <td>
        {{$row->phone}}
      </td>
      <td>
        {{$row->address}}
      </td>
      <td>
        {{$row->memberId}}
      </td>
      <td>
        {{$row->membership}}
      </td>
      <td>
          @if ($row->member_verify == 2)
           <span class="badge badge-pill badge-success">Paid</span>
           @else
           <span class="badge badge-pill badge-danger">Unpaid</span>
          @endif
      </td>
      
      <td class="text-right">
        {{date('M d, Y', strtotime($row->created_at))}}
      </td>
    
    </tr> 
    @endforeach
    @endif
   </tbody>
   
  </table>
  {!! $data->links() !!}
 