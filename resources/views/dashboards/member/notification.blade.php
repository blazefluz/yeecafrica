@extends('../../partials/app')

@section('sidebar')
@include('partials.memberSidebar')
@endsection

@section('navbar')
@include('partials.header')
@endsection

@section('content')

    <div class="row">
      
      <div class="col-md-10">
        <div class="card card-plain">
          <div class="card-header">
            <h5 class="card-title">Notification</h5>
          </div>
          {{-- noti  --}}
            <div class="card card-body">
                <div class="card-header">
                    <span class="badge badge-pill badge-success">Registration Successful</span>
                </div>
                <div class="card-body">
                    <p>Welcome <strong>{{$user->fname}} </strong>to YEEC Africa your registration was successful. Please update your profile to continue 
                        <a href="{{url('/u/profile')}}">click here to update</a>
                    </p>
                    <h6>
                    <i class="fa fa-clock-o"></i> Just Now
                    </h6>
                </div>
            </div>
        {{-- noti  --}}

          {{-- noti  --}}
            <div class="card card-body">
                <div class="card-header">
                    <span class="badge badge-pill badge-danger">Upgrade your account</span>
                </div>
                <div class="card-body">
                    <p>Hello <strong>{{$user->fname}}</strong>, to have full access to all the benefits as a member, you need to pay your dues to continue 
                        <a href="{{url('/u/payment')}}">click here to continue</a>
                    </p>
                    <h6>
                    <i class="fa fa-clock-o"></i> Just Now
                    </h6>
                </div>
            </div>
        {{-- noti  --}}
           
        </div>
      </div>
    </div>


@endsection
