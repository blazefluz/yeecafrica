@extends('../../partials/app')

@section('sidebar')
@include('partials.memberSidebar')
@endsection

@section('navbar')
@include('partials.header')
@endsection

@section('content')

    <div class="row">
      
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title">Subscription</h5>
          </div>
            <div class="card-body">
                <div class=" table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Plan</th>
                                <th>Amount</th>
                                <th>Reference Code</th>
                                <th>Status</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                      <tbody>
                          @php
                              $i = 1;
                          @endphp
                        @foreach ($subscription as $sub)
                        
                        <tr>
                         <td>{{$i++}}</td>
                          <td class="text-left">{{$sub->payment_plan}}</td>
                          <td>{{ "₦ ".number_format(substr($sub->amount, 0, -2))}}</td>
                          <td>{{$sub->reference}}</td>
                          <td>{{$sub->status}}</td>
                          <td>{{date('M d, Y', strtotime($sub->created_at))}}</td>
                        </tr>
                        
                        @endforeach
                      
                      </tbody>
                    </table>
                  </div>
            </div>
        </div>
      </div>
    </div>


@endsection
