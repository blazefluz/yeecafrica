@extends('../../partials/app')

@section('sidebar')
@include('partials.memberSidebar')
@endsection

@section('navbar')
@include('partials.header')
@endsection

@section('content')

    <div class="row">
      <div class="col-md-3">
        <div class="card text-center">
          <img src="{{asset('/seed.jpg')}}" alt="seed">
          <div class="card-body">
           <p class="card-title">Yeec Seed Capital program</p>
          <div class="card-footer m-0">
            <hr>
            <div class="button-container text-center">
              <div class="row">
                <div class="col-lg-12 col-md-12 m-0 p-0 ">
                  @php
                     $program_check = DB::table('program_form')->where(['user_id' =>$user->id , 'program_id' => 1])->get();
                  @endphp
                  @if (count($program_check) > 1)
                  <a class="btn btn-success" href="#">Submitted Successfully</a>
                  @else 
                   <a class="btn btn-warning" href="seed-catipal/application">Apply Now</a>
                  @endif
                </div>
                
                
              </div>
            </div>
          </div>
         </div>
        </div>
       
      </div>
      <div class="col-md-3">
        <div class="card text-center">
          <img src="{{asset('/oil.jpg')}}" alt="oil">
          <div class="card-body">
           <p class="card-title">YEEC Oil And Gas Training and Certification</p>
          <div class="card-footer">
            <hr>
            <div class="button-container">
              <div class="row">
                <div class="col-lg-12 col-md-12  m-0 p-0 ">
                  @php
                     $program_check = DB::table('program_form')->where(['user_id' =>$user->id , 'program_id' => 1])->get();
                  @endphp
                  @if (count($program_check) > 1)
                  <a class="btn btn-success" href="#">Submitted Successfully</a>
                  @else 
                   <a class="btn btn-warning" href="seed-catipal/application">Apply Now</a>
                  @endif
                </div>
                
                
              </div>
            </div>
          </div>
         </div>
        </div>
       
      </div>
     
    </div>


@endsection
