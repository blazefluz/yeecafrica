@extends('../../partials/app')

@section('sidebar')
@include('partials.memberSidebar')
@endsection

@section('navbar')
@include('partials.header')
@endsection

@section('content')

    <div class="row">
   
      <div class="col-md-6">
        <div class="card card-user">
          <div class="card-header">
            <h5 class="card-title">Upgrade Account</h5>
          </div>
          <div class="card-body">
            <form method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
                <div class="row" style="margin-bottom:40px;">
                  <div class="col-md-12 col-md-offset-2">
                    <p>
                        <div>
                           Pay your membership dues
                        </div>
                    </p>
                    <input type="hidden" name="first_name" value="{{$user->fname}}"> {{-- required --}}
                    <input type="hidden" name="last_name" value="{{$user->lname}}"> {{-- required --}}
                    <input type="hidden" name="email" value="{{$user->email}}"> {{-- required --}}
                    <input type="hidden" name="orderID" value="345">
                    <div class="col-md-12 pr-1">
                        <div class="form-group">
                            <select class="form-control" name="plan" id="">
                                @foreach ($plans as $plan)
                                <option value="{{$plan->code}}">{{$plan->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    {{-- <input type="hidden" name="amount" value="800"> required in kobo --}}
                    <input type="hidden" name="metadata" value="{{ json_encode($array = ['first_name' => $user->fname, 'last_name' => $user->lname,
                        
                    ]) }}" > 
                    <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> {{-- required --}}
                    <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}"> {{-- required --}}
                    {{ csrf_field() }} {{-- works only when using laravel 5.1, 5.2 --}}
        
                     <input type="hidden" name="_token" value="{{ csrf_token() }}"> {{-- employ this in place of csrf_field only in laravel 5.0 --}}
        
        
                    <p>
                      <button class="btn btn-success btn-lg btn-block" type="submit" value="Pay Now!">
                      <i class="fa fa-plus-circle fa-lg"></i> Pay Now!
                      </button>
                    </p>
                  </div>
                </div>
        </form>
          
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card card-plain">
          <div class="card-header">
            <h5 class="card-title">Membership Dues (Anually)</h5>
          </div>
          <div class="card-body ">
            <div class=" table-responsive">
              <table class="table">
                <tbody>
                  @foreach ($plans as $plan)

                  <tr>
                    <td class="text-left">{{$plan->name}}</td>
                    <td>{{ "₦ ".number_format(substr($plan->amount, 0, -2))}}</td>
                  </tr>
                  
                  @endforeach
                
                </tbody>
              </table>
            </div>
          </div>
        </div>
        
      </div>
    </div>


@endsection
