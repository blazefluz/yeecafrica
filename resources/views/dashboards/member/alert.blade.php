@extends('../../partials/app')

@section('sidebar')
@include('partials.memberSidebar')
@endsection

@section('navbar')
@include('partials.header')
@endsection

@section('content')

    <div class="row">
    
      <div class="col-md-12">
        <div class="card card-user">
          <div class="card-header">
            <h5 class="card-title">Congratulation!!!</h5>
          </div>
          <div class="card-body">
           <p>Congratulation your form has been submitted and it is currently been processed.</p>
          </div>
        </div>
      </div>
    </div>


@endsection
