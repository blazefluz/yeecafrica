@extends('../../../partials.app')
@section('navbar')
@include('partials/header')
@endsection

@section('content')
<style>

.main-panel{
    width: 100%;
}
.create-form{
    margin: auto;
}
</style>
    <div class="col-md-8 create-form">
        <div class="card card-user" >
          <div class="card-header">
            <h5 class="card-title text-center">Membership Form  (STEP TWO)</h5>
    
          </div>
          <div class="card-body" style="margin:auto">
              
          
            <form  action="/u/create-step2" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="col-md-6 col-sm-12" style="margin:auto">
                    <span style="margin:auto">Upload Your Profile Image</span><br/>
                    <br>
                    <div class="fileinput text-center fileinput-new" data-provides="fileinput">
                      <div class="fileinput-new thumbnail">
                        <img src=
                        @if(!isset($memberForm->memberFormImg))
                        {{asset('assets/img/image_placeholder.jpg')}}
                        @else
                        {{asset('/storage/'.$memberForm->memberFormImg) }}
                        @endif
                        
                         alt="...">
                      </div>
                      <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
                      <div>
                        <span class="btn btn-rose btn-round btn-file">
                          <span class="fileinput-new">Select image</span>
                          <span class="fileinput-exists">Change</span>
                          <input type="hidden" value="" name="..."><input type="file" {{ (!empty($memberForm->memberFormImg)) ? "disabled" : ''}} name="memberFormimg" id="memberFormimg" >
                        </span>
                      
                        <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                      </div>
                    </div>
                  </div>
                  
                <div class="form-group">
                    {{-- <input type="file" {{ (!empty($memberForm->memberFormImg)) ? "disabled" : ''}} class="form-control-file" aria-describedby="fileHelp"> --}}
                    
                    <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
                </div>
                <div class="update " >
                    <a type="button" href="/u/create-step1" class="btn btn-warning">Back to Step 1</a>
                    <button type="submit" class="btn btn-primary btn-round">Save & Continue to step three(3)</button>
                </div>
            </form><br/>
                @if(isset($memberForm->memberFormImg))
                <form action="/u/remove-image" method="post">
                    {{ csrf_field() }}
                <button type="submit" class="btn btn-danger">Remove Image</button>
                </form>
                @endif
                {{-- <button type="submit" class="btn btn-primary">Review memberForm Details</button> --}}
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
               
           
          </div>
        </div>
      </div>
@endsection

