@extends('../../../partials.app')
@section('navbar')
@include('partials/header')
@endsection

@section('content')
<style>

.main-panel{
    width: 100%;
}
.create-form{
    margin: auto;
}
</style>
    <div class="col-md-8 create-form">
        <div class="card card-user">
          <div class="card-header">
            <h5 class="card-title text-center">Membership Form (STEP ONE)</h5>
            @php
                $ngStates = new Coderatio\NGStates\NGStates(); 
            @endphp
           
          </div>
          <div class="card-body">
            <form method="POST" action="{{url('/u/create-step1')}}" >
                @csrf
                <div class="row">
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                          <label>First Name <span style="color: red">*</span></label>
                          <input type="text" class="form-control @error('fname') is-invalid @enderror" name="fname" placeholder="First Name"  value="{{$user->fname}}">
                          @error('fname')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                    </div>
                    <div class="col-md-4 pl-1">
                        <div class="form-group">
                        <label>Middle Name</label>
                        <input type="text" class="form-control  @error('mname') is-invalid @enderror" name="mname" placeholder="Middle Name" value="{{$memberForm['mname']}}">
                        @error('mname')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                    <div class="col-md-4 pl-1">
                        <div class="form-group">
                        <label>Last Name <span style="color: red">*</span></label>
                        <input type="text" class="form-control  @error('lname') is-invalid @enderror" name="lname" placeholder="Last Name" value="{{$user->lname}}">
                        @error('lname')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group">
                    <label>Phone <span style="color: red">*</span></label>
                    <input type="number" class="form-control  @error('phone') is-invalid @enderror" @ name="phone"  value="{{ $user->phone}}">
                    @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address <span style="color: red">*</span></label>
                    <input type="email" class="form-control  @error('email') is-invalid @enderror" disabled value="{{{$user->email}}}" placeholder="Email">
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group">
                    <label>DOB  <span style="color: red">*</span></label>
                    <input type="text" id='datetimepicker1' class="form-control date  @error('dob') is-invalid @enderror" name="dob"  value="@if (isset($memberForm['dob'])){{{$memberForm['dob']}}} @else {{'10/05/1945'}} @endif ">
                    @error('dob')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-3 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Sex <span style="color: red">*</span></label>
                    <select name="sex"  class="form-control" id="">
                        <option {{{ (isset($memberForm->sex) && $memberForm->sex == 'male') ? "selected=\"selected\"" : "" }}} value="male">Male</option>
                        <option {{{ (isset($memberForm->sex) && $memberForm->sex == 'female') ? "selected=\"selected\"" : "" }}} value="female">Female</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-3 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Religion <span style="color: red">*</span></label>
                    <select name="religion"  class="form-control" id="">
                        <option {{{ (isset($memberForm->religion) && $memberForm->religion == 'Christain') ? "selected=\"selected\"" : "" }}} value="male">Christain</option>
                        <option {{{ (isset($memberForm->religion) && $memberForm->religion == 'Islam') ? "selected=\"selected\"" : "" }}} value="female">Islam</option>
                    </select>
                  </div>
                </div>
              </div> 

              
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Address <span style="color: red">*</span></label>
                    <input type="text" class="form-control  @error('address') is-invalid @enderror" name="address" placeholder="Home Address" value="{{$memberForm['address']}}">
                    @error('address')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 pr-1">
                  <div class="form-group">
                    <label>City <span style="color: red">*</span></label>
                    <input type="text" class="form-control  @error('city') is-invalid @enderror" name="city" required placeholder="City" value="{{$memberForm['city']}}">
                    @error('city')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                    <div class="form-group" >
                      <label>State <span style="color: red">*</span></label>
                      <select name="state" id="state" required class="form-control">
                        <option>Select State</option>
                          @foreach ($ngStates->getStates() as $state)
                            <option {{{ (isset($memberForm->state) && $memberForm->state == $state->name) ? "selected=\"selected\"" : "" }}} value="{{$state->name}}">{{$state->name}}</option>
                          @endforeach
                      </select>
                        
                    </div>
                </div>
            

              </div>
              <div class="row">
                
                <div class="col-md-6">
                    <div class="form-group">
                      <label>LGA/Province <span style="color: red">*</span></label>
                      <select name="lga" required class="form-control" id="lga" >
                          <option>Select LGA</option>
                      </select>
                     
                    </div>
                </div>
                <div class="col-md-3 pl-1">
                  <div class="form-group">
                    <label>Zip  <span style="color: red">*</span></label>
                    <input type="text" name="zipcode" class="form-control" id="" placeholder="+234" value="+234">
                  </div>
                </div>
                <div class="col-md-3 pl-1">
                  <div class="form-group">
                    <label>Country  <span style="color: red">*</span></label>
                    <select name="country" class="form-control" id="">
                        <option value="{{__('Nigeria')}}">{{__('Nigeria')}}</option>
                    </select>
                  </div>
                </div>

              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>About Me</label>
                    <textarea class="form-control textarea " name="about">{{$memberForm['about']}}</textarea>
                    
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="update ml-auto mr-auto">
                  <button type="submit" class="btn btn-primary btn-round">Save & Continue to Step Two(2)</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
@endsection
@section('script')
    
    <script>
        
        $( document ).ready(function() {
            $('#state').on('change', function(){
                let lga = $('#state').val();
                $.ajax({
                    url: '/lga',
                    type: 'post',
                    cache: false,
                    data: { "_token": "{{ csrf_token() }}", 'lga': lga },
                    success: function(data){
                        console.log("Success!");
                        $('#lga').html(data);
                    },
                    error: function(xhr, textStatus, thrownError){
                        alert('Somethin went wrong');
                    }
                });

            });
        });
    </script>
@endsection