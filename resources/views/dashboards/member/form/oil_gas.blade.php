@extends('../../../partials.app')


@section('sidebar')
@include('partials.memberSidebar')
@endsection

@section('navbar')
@include('partials/header')
@endsection

@section('content')

    <div class="col-md-12">
        <div class="card card-user">
          <div class="card-header">
            <h5 class="card-title">YEEC OIL AND GAS TRAINING AND CERTIFICATION </h5>
           
           
          </div>
          <div class="card-body">
            <form method="post" enctype="multipart/form-data" action="{{url('/u/oil-gas/application/store')}}" >
                @csrf
                <div class="row">
                    <div class="col-md-6  ">
                        <div class="form-group">
                          <label>Academic Qualifications? <span style="color: red">*</span></label>
                          <input type="text" name="academicqual" class="form-control">
                          @error('academicqual')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                    </div>
                    <div class="col-md-6  ">
                        <div class="form-group">
                          <label>Will you be available for the training? 
                            <span style="color: red">*</span></label>
                          <select name="pavalability" id="pavalability" class="form-control">
                              <option value="Nes">Yes</option>
                              <option value="No">No</option>
                              <option value="Subject to rescheduling">Subject to rescheduling</option>
                          </select>
                          @error('pavalability')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                    </div>
                  
                  
                </div>
              
                <div class="row">
                    <div class="col-md-12  ">
                        <div class="form-group">
                          <label>In no more than 200 words, explain how this training will benefit you * 
                            <span style="color: red">*</span></label>
                            <textarea name="pbenefit" id="" class="form-control"></textarea>
                 
                        </div>
                    </div>
                    <div class="col-md-12  ">
                        <div class="form-group">
                          <label>What skills do you hope to gain from this training *
                            <span style="color: red">*</span></label>
                            <textarea name="pskillsgain" id="" class="form-control"></textarea>
                 
                        </div>
                    </div>
                    <div class="col-md-4  ">
                        <div class="form-group">
                          <label>UPLOAD LETTER OF ADMISSION
                            <span style="color: red">*</span></label>
                          <br>
                              <div class="fileinput text-center fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail">
                                  <img src= {{asset('assets/img/image_placeholder.jpg')}}
                                 
                                   alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
                                <div>
                                  <span class="btn btn-rose btn-round btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="hidden" value="" name="..."><input type="file" name="admission_letter_image"  >
                                  </span>
                                
                                  <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                </div>
                              </div>
                            </div>
                    </div>
                    <div class="col-md-4  ">
                        <div class="form-group">
                          <label>UPLOAD YOUR ACCEPTANCE LETTER
                            <span style="color: red">*</span></label>
                          <br>
                              <div class="fileinput text-center fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail">
                                  <img src= {{asset('assets/img/image_placeholder.jpg')}}
                                 
                                   alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
                                <div>
                                  <span class="btn btn-rose btn-round btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="hidden" value="" name="..."><input type="file" name="acceptance_letter_image"  >
                                  </span>
                                
                                  <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                </div>
                              </div>
                            </div>
                    </div>
                    <div class="col-md-4  ">
                        <div class="form-group">
                          <label>UPLOAD YOUR FIRST DEGREE / EQUIVALENT CERTIFICATE
                            <span style="color: red">*</span></label>
                          <br>
                              <div class="fileinput text-center fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail">
                                  <img src= {{asset('assets/img/image_placeholder.jpg')}}
                                 
                                   alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
                                <div>
                                  <span class="btn btn-rose btn-round btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="hidden" value="" name="..."><input type="file" name="degree_cert_image"  >
                                  </span>
                                
                                  <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                </div>
                              </div>
                            </div>
                    </div>
                   
                  
                </div>
                <div class="row">
                   
                    <div class="col-md-12  ">
                        <div class="form-group">
                         <p>
                           <strong>ATTESTATION:</strong> 
                           <br>
                           By signing my first and last name at the end of 
                           this form I hereby certify that the above information is true 
                           and correct to the best of my knowledge. I fully understand 
                           "YEEC OIL AND GAS TRAINING AND CERTIFICATION" Mission Statement 
                           and I acknowledge that a majority of contributions and donations 
                           collected by the YEEC AFRICA will be used to fund the 
                           "YEEC OIL AND GAS TRAINING AND CERTIFICATION" programme.
                         </p>
                        </div>
                    </div>
                  
                </div>
                <div class="row">
                    <div class="update ml-auto mr-auto">
                      <button type="submit" class="btn btn-primary btn-round">Submit Application</button>
                    </div>
                  </div>
            </form>
          </div>
        </div>
      </div>
@endsection
