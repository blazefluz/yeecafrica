@extends('../../../partials.app')
@section('navbar')
@include('partials/header')
@endsection

@section('content')
<style>

.main-panel{
    width: 100%;
}
.create-form{
    margin: auto;
}
</style>
    <div class="col-md-8 create-form">
        <div class="card card-user">
          <div class="card-header">
            <h5 class="card-title text-center">Membership Form  (STEP THREE)</h5>
            @php
                $ngStates = new Coderatio\NGStates\NGStates(); 
            @endphp
           
          </div>
          <div class="card-body">
            <form method="POST" action="{{url('/u/create-step3')}}" >
                @csrf
                <div class="row">
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                          <label>Occupation  <span style="color: red">*</span></label>
                          <input type="text" class="form-control @error('occupation') is-invalid @enderror" name="occupation" placeholder="e.g teacher"  value="{{$user->occupation}}">
                          @error('occupation')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                    </div>
                    <div class="col-md-6 pl-1">
                        <div class="form-group">
                        <label>Organization</label>
                        <input type="text" class="form-control  @error('organization') is-invalid @enderror" name="organization" placeholder="Organization" value="{{$memberForm['organization']}}">
                        @error('organization')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                  
                </div>
              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group">
                    <label>Degree  <span style="color: red">*</span></label>
                    <input type="text" class="form-control  @error('degree') is-invalid @enderror" name="degree" placeholder="E.g Bsc"  value="{{ $memberForm['degree'] }}">
                    @error('degree')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label for="institution">Institution  <span style="color: red">*</span></label>
                    <input type="institution" class="form-control  @error('institution') is-invalid @enderror" placeholder="e.g University of Port Harcourt" value="{{{$user->institution}}}" name="institution">
                    @error('institution')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
              </div>
           

              
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Matric No</label>
                    <input type="text" class="form-control  @error('matricno') is-invalid @enderror" name="matricno" placeholder="" value="{{$memberForm['matricno']}}">
                    @error('matricno')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Year of Graduation  <span style="color: red">*</span></label>
                    <input type="text" class="form-control  @error('yearg') is-invalid @enderror" placeholder="e.g 2014" name="yearg" placeholder="" value="{{$memberForm['yearg']}}">
                    @error('yearg')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 ">
                  <div class="form-group">
                    <label>Any Other Degree</label>
                    <input type="text" class="form-control  @error('aodegree') is-invalid @enderror" name="aodegree"   value="{{$memberForm['aodegree']}}">
                    @error('aodegree')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="checkbox" class="form-check-input" value="1" name="fieldinterest[]">
                      <span class="form-check-sign"></span>Internship and Employment                                              
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="checkbox" class="form-check-input" value="2" name="fieldinterest[]">
                      <span class="form-check-sign"></span>Seed Capital for Entrepreneurial Development                                              
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="checkbox" class="form-check-input" value="3" name="fieldinterest[]">
                      <span class="form-check-sign"></span>Scholarship Award Program                                               
                    </label>
                  </div>
                 
                  
                </div>
                <div class="col-md-6 ">
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="checkbox" class="form-check-input" value="5" name="fieldinterest[]">
                      <span class="form-check-sign"></span>Software Development Training                                              
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="checkbox" class="form-check-input" value="6" name="fieldinterest[]">
                      <span class="form-check-sign"></span>Business Development and Job Creation                                              
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="checkbox" class="form-check-input" value="7" name="fieldinterest[]">
                      <span class="form-check-sign"></span>Oil and Gas Training and Certification                                               
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="checkbox" class="form-check-input" value="8" name="fieldinterest[]">
                      <span class="form-check-sign"></span>Student Industrial Attachment                                                                                         
                    </label>
                  </div>
                  
                </div>
            

              </div>
              <div class="row">
                <div class="update ml-auto mr-auto">
                  <button type="submit" class="btn btn-primary btn-round">Save & Continue</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
@endsection
@section('script')
    
    <script>
        
        $( document ).ready(function() {
            $('#state').on('change', function(){
                let lga = $('#state').val();
                $.ajax({
                    url: '/lga',
                    type: 'post',
                    cache: false,
                    data: { "_token": "{{ csrf_token() }}", 'lga': lga },
                    success: function(data){
                        console.log("Success!");
                        $('#lga').html(data);
                    },
                    error: function(xhr, textStatus, thrownError){
                        alert('Somethin went wrong');
                    }
                });

            });
        });
    </script>
@endsection