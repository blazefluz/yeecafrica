@extends('../../../partials.app')
@section('navbar')
@include('partials/header')
@endsection

@section('content')
<style>

.main-panel{
    width: 100%;
}
.create-form{
    margin: auto;
}
</style>
    <div class="col-md-8 create-form">
        <div class="card card-user" >
          <div class="card-header">
            <h5 class="card-title text-center">Membership Form (STEP FOUR)</h5>
    
          </div>
          <div class="card-body" style="margin:auto">
              <p>
                By signing my First and Last name at the end of this form, I hereby certify that the above information is true and correct to the best of my knowledge. I fully understand by submitting this membership form that I agree to conduct myself in accordance with Young Engineers Entrepreneurship Club code of ethics and by-laws. 
                Failure to do so will result in suspension of my membership with no refund.
                
              </p>
              <form action="/u/submission" method="POST">
                @csrf
                <div class="row">
                    <div class="update ml-auto mr-auto">
                      <button type="submit" class="btn btn-primary btn-round">Save</button>
                    </div>
                  </div>
              </form>
          </div>
        </div>
      </div>
@endsection

