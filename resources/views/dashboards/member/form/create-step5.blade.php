@extends('../../../partials.app')
@section('navbar')
@include('partials/header')
@endsection


@section('content')
<style>

    .main-panel{
        width: 100%;
    }
   
    </style>
    <div class="col-md-12 create-form">
        <div class="">
            <h5 class="card-title text-center">Membership Form (FINAL)  <span style="color:red">Activate Account</span>  </h5>
               <br>
          </div>
        <div class="row  ">
       
         
          <div class="col-md-8">
            <div class="card">
              <div class="card-header">
                <h5 class="card-title"> <strong>MEMBERSHIPS BENEFITS</strong></h5>
              </div>
              <div class="card-body ">
               
            A Member of YEEC shall be entitled to the following benefits depending on the underlisted
            categories.
            
            <strong>Student Membership (N3,000 or $9.8)</strong>
            <ol>
                 <li>Access to YEEC’s programs and events.</li>
                 <li>Access to internship opportunities.</li>
                 <li>Access to scholarship opportunities.</li>
            </ol>
            <strong>Graduate Membership (N5,000 or $16.33)</strong>
            <ol>
                 <li> Access to YEEC programs and events</li>
                 <li>Access to training and certifications</li>
                 <li>Access to Job and Career Networking Opportunities</li>
            </ol>
            <strong>Professional (Engineering) Membership (N10,000 or $32.7)</strong>
            <ol>
                 <li>Access to YEEC programs and events</li>
                 <li>Participate in the Seed Capital for Entrepreneurship Development</li>
                 <li>Can represent YEEC overseas</li>
                 <li>Nomination to serve in a Leadership role</li>
            </ol>
            <strong>Associate - Student (N4,000 or $13.07)</strong>
            <ol>
                 <li>Participate in special trainings</li>
                 <li>Participate in the Seed Capital for Entrepreneurship Development</li>
                 <li>Access to YEEC programs and events</li>
            </ol>
            <strong>Associate - Graduate (N7,000 or $22.87)</strong>
            <ol>
                 <li>Access to YEEC programs and events</li>
                 <li>Participate in special trainings</li>
                 <li>Participate in the Seed Capital for Entrepreneurship Development</li>
            </ol>
            <strong>Associate Professionals of non-engineering discipline (N12,000 or $39.21)</strong>
            <ol>
                 <li> Access to YEEC programs and events</li>
                 <li>Participate in special trainings</li>
                 <li>Participate in the Seed Capital for Entrepreneurship Development</li>
            </ol>
            <strong>Fellowship - Silver (N300,000 or $980.39)</strong>
            <ol>
                 <li>Receive appreciation plaque</li>
                 <li>Nomination of 2 persons for undergraduate scholarship for one year.</li>
            </ol>
            <strong>Fellowship – Gold (N500,000 or $1,633.99)</strong>
            <ol>
                 <li>Nomination of 5 persons for Scholarship; 3 undergraduates and 2 Master’s Degree for one
            year.</li>
                 <li>Receive appreciation plaque</li>
            </ol>
            <strong>Fellowship – Diamond (N1,000,000 or $3267.97)</strong>
            <ol>
                 <li>Nomination of 10 persons for scholarship award for one year; 6 Undergraduates and 4
            Master’s Degree.</li>
                 <li>Fellowship decoration</li>
                 <li>Plaque presentation by top personalities.</li>
            </ol>
            <strong>Fellowship – Platinum (N5,000,000 or $16,339.87)</strong>
            <ol>
                 <li>Nomination of 15 persons for scholarship award for one year; 10 undergraduates
            and 5 Master’s Degree.</li>
                 <li>Decoration as EAGLE YEEC</li>
                 <li>Presentation of gold-plated appreciation award</li>
                 <li>Branded as YEEC Ambassador</li>
            </ol>
            <strong>Corporate – Silver (N1,500,000 or $4,901.96)</strong>
            <ol>
                 <li> Nomination of 3 Undergraduates and 2 Master's Degree for one-year scholarship award.</li>
                 <li>Nomination of participants for YEEC’s programmes and events.</li>
                 <li>Nomination of 1 participant for YEEC’s training and certification programmes.</li>
            </ol>
            <strong>Corporate – Gold (N3,000,000 or $9,803.92)</strong>
            <ol>
                 <li>Nomination of 4 Undergraduates and 3 Master's Degree for one-year scholarship award.</li>
                 <li>Nomination of participants for YEEC’s programmes and events</li>
                 <li>Nomination of 2 participants for YEEC’s trainings and certification programs.</li>
            </ol>
            <strong>Corporate – Diamond (N5,000,000 or $16,339.87)</strong>
            <ol>
                 <li>Nomination of 7 Undergraduates and 3 Master's Degree for one-year scholarship award</li>
                 <li>Nomination of participants for YEEC’s events.</li>
                 <li>Nomination of 4 participants for YEEC’s training and certification programs.</li>
                 <li>Access to platform to talk about their organization at all YEEC’s events and functions</li>
            </ol>
            <strong>Corporate – Platinum (N10,000,000 or $32,679.74)</strong>
            <ol>
                 <li>Recognised as Eagle Member of the club</li>
                 <li>Decoration as Corporate Patron of the club</li>
                 <li>Nomination of 14 Undergraduates and 6 Master’s Degree holders for scholarship</li>
                 <li>Nomination of participants for YEEC’s programmes and events.</li>
                 <li>Nomination of 5 participants for training and certification programs</li>
                 <li>Receive Gold Plated Appreciation Plaque.</li>
                 <li>Branded on YEEC’s Ambassador</li>
            </ol>
                
                
              </p>
              </div>
            </div>
            
          </div>
          <div class="col-md-4">
            <div class="card card-user" style="background-color:#ffcc00">
              <div class="card-header">
                <h6 class="card-title">Pay Your Membership Dues (Anually)</h6>
              </div>
              <div class="card-body">
                <form method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
                    <div class="row" style="margin-bottom:40px;">
                      <div class="col-md-12 col-md-offset-2">
                        <p>
                            <div>
                               Select your account type and pay to activate your account
                            </div>
                        </p>
                        <input type="hidden" name="first_name" value="{{$user->fname}}"> {{-- required --}}
                        <input type="hidden" name="last_name" value="{{$user->lname}}"> {{-- required --}}
                        <input type="hidden" name="email" value="{{$user->email}}"> {{-- required --}}
                        <input type="hidden" name="orderID" value="345">
                        <div class="col-md-12 pr-1">
                            <div class="form-group">
                                <select class="form-control" name="plan" id="">
                                    @foreach ($plans as $plan)
                                    <option value="{{$plan->code}}">{{$plan->name}} - {{ "₦ ".number_format(substr($plan->amount, 0, -2))}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        {{-- <input type="hidden" name="amount" value="800"> required in kobo --}}
                        <input type="hidden" name="metadata" value="{{ json_encode($array = ['first_name' => $user->fname, 'last_name' => $user->lname,
                            
                        ]) }}" > 
                        <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> {{-- required --}}
                        <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}"> {{-- required --}}
                        {{ csrf_field() }} {{-- works only when using laravel 5.1, 5.2 --}}
            
                         <input type="hidden" name="_token" value="{{ csrf_token() }}"> {{-- employ this in place of csrf_field only in laravel 5.0 --}}
            
            
                        <p>
                          <button class="btn  btn-lg btn-block" type="submit" value="Pay Now!">
                          <i class="fa fa-plus-circle fa-lg"></i> Pay Now!
                          </button>
                        </p>
                      </div>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
    </div>


@endsection
