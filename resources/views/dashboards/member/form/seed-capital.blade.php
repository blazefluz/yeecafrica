@extends('../../../partials.app')


@section('sidebar')
@include('partials.memberSidebar')
@endsection

@section('navbar')
@include('partials/header')
@endsection

@section('content')

    <div class="col-md-12">
        <div class="card card-user">
          <div class="card-header">
            <h5 class="card-title">Seed Capital Form </h5>
            @php
                $ngStates = new Coderatio\NGStates\NGStates(); 
            @endphp
           
          </div>
          <div class="card-body">
            <form method="post" enctype="multipart/form-data" action="{{url('/u/seed-catipal/application/store')}}" >
                @csrf
                <div class="row">
                    <div class="col-md-4  ">
                        <div class="form-group">
                          <label>Do you have a business plan? <span style="color: red">*</span></label>
                          <select name="bplan" id="bplan" class="form-control">
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                              <option value="Others">I don't Know What a business plan is.</option>
                          </select>
                          @error('bplan')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                    </div>
                    <div class="col-md-4  ">
                        <div class="form-group">
                          <label>Do you have a business account?
                            <span style="color: red">*</span></label>
                          <select name="baccount" id="baccount" class="form-control">
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                          </select>
                          @error('baccount')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                    </div>
                    <div class="col-md-4  ">
                        <div class="form-group">
                          <label>Do you have bookkeeping skills?
                            <span style="color: red">*</span></label>
                          <select name="bookkeepingskills" id="bookkeepingskills" class="form-control">
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                          </select>
                          @error('bookkeepingskills')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                    </div>
                  
                </div>
                <div class="row">
                    <div class="col-md-4  ">
                        <div class="form-group">
                          <label>What do you require the non-returnable sum of =N=1,500, 000 for? <span style="color: red">*</span></label>
                          <select name="capitalneeds" id="capitalneeds" class="form-control">
                              <option value="business Start up">Business Start up</option>
                              <option value="Business Scale up/ Expansion">Business scale up/ Expansion</option>
                              <option value="Purchase of equipment and Machinery">Purchase of equipment and Machinery</option>
                              <option value="To learn a skill">To learn a skill</option>
                          </select>
                          @error('capitalneeds')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                    </div>
                    
                    <div class="col-md-4  ">
                        <div class="form-group">
                          <label>Open to business milestone monitoring and evaluation
                            <span style="color: red">*</span></label>
                          <select name="bmilestone" id="bmilestone" class="form-control">
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-4  ">
                        <div class="form-group">
                          <label>Means Identification 
                            <span style="color: red">*</span></label>
                          <select name="identification_type" id="identification_type" class="form-control">
                              <option value="International Passport">International Passport</option>
                              <option value="Drivers Licence">Drivers Licence</option>
                              <option value="Voter's card">Voter's card</option>
                          </select>
                        </div>
                    </div>
                  
                </div>
                <div class="row">
                   
                    <div class="col-md-12  ">
                        <div class="form-group">
                          <label>Upload valid means of Identification ( International Passport, Drivers Licence, Voter's card)
                            <span style="color: red">*</span></label>
                          <br>
                              <div class="fileinput text-center fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail">
                                  <img src= {{asset('assets/img/image_placeholder.jpg')}}
                                 
                                   alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
                                <div>
                                  <span class="btn btn-rose btn-round btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="hidden" value="" name="..."><input type="file" name="identification_image" id="identification_image" >
                                  </span>
                                
                                  <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                </div>
                              </div>
                            </div>
                    </div>
                    <div class="col-md-12  ">
                        <div class="form-group">
                          <label>Explain in less than 100 words, how you intend to utilize the non returnable seed capital sum of =N=1,500,000.
                            <span style="color: red">*</span></label>
                            <textarea name="descfundusage" id="" class="form-control"></textarea>
                 
                        </div>
                    </div>
                  
                </div>
                <div class="row">
                   
                    <div class="col-md-12  ">
                        <div class="form-group">
                         <p>
                           <strong>Attestation</strong> 
                           <br>
                            By signing my first and last name at the end of this form, I hereby certify that the above information is true and correct to the best of my knowledge. I fully understand by submitting this application form.I understand the YEEC seed capital for entrepreneurship development program objectives. I also agree  to conduct myself in accordance with the Young Engineer's Enterpreneurship Club code of ethics and by-laws. Failure to do so will result in suspension
                             of my application for the My Money, My Business grant.
                         </p>
                        </div>
                    </div>
                  
                </div>
                <div class="row">
                    <div class="update ml-auto mr-auto">
                      <button type="submit" class="btn btn-primary btn-round">Submit Application</button>
                    </div>
                  </div>
            </form>
          </div>
        </div>
      </div>
@endsection
@section('script')
    
    <script>
        
        $( document ).ready(function() {
            $('#state').on('change', function(){
                let lga = $('#state').val();
                $.ajax({
                    url: '/lga',
                    type: 'post',
                    cache: false,
                    data: { "_token": "{{ csrf_token() }}", 'lga': lga },
                    success: function(data){
                        console.log("Success!");
                        $('#lga').html(data);
                    },
                    error: function(xhr, textStatus, thrownError){
                        alert('Somethin went wrong');
                    }
                });

            });
        });
    </script>
@endsection