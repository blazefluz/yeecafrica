@extends('../../partials/app')

@section('sidebar')
@include('partials.memberSidebar')
@endsection

@section('navbar')
@include('partials.header')
@endsection

@section('content')

    <div class="row">
      
      <div class="col-md-10">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title">Settings</h5>
          </div>
            <div class="card-body">
                <div class="row">
                  <div class="col-lg-4 col-md-5 col-sm-4 col-6">
                    <div class="nav-tabs-navigation verical-navs">
                      <div class="nav-tabs-wrapper">
                        <ul class="nav nav-tabs flex-column nav-stacked" role="tablist">
                       
                          <li class="nav-item">
                            <a class="nav-link" href="#description" role="tab" data-toggle="tab" >Change password</a>
                          </li>
                         
                          <li class="nav-item">
                            <a class="nav-link" href="#support" role="tab" data-toggle="tab" aria-selected="false" >Support</a>
                          </li>
                          
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-8 col-md-7 col-sm-8 col-6">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      
                      <div class="tab-pane active show" id="description">
                        <form method="POST" action="update" >
                            @csrf
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                      <label>Old Password</label>
                                      <input type="text" class="form-control @error('fname') is-invalid @enderror" name="fname" placeholder="********"  value="">
                                      @error('fname')
                                          <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                          </span>
                                      @enderror
                                    </div>
                                </div>
                           
                            </div>
                          <div class="row">
                            <div class="col-md-12 ">
                              <div class="form-group">
                                <label>New Password</label>
                                <input type="number" class="form-control  @error('phone') is-invalid @enderror" name="phone" placeholder="*****" value="">
                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                            </div>
                          </div>
        
                          <div class="row">
                            <div class="update ml-auto mr-auto">
                              <button type="submit" class="btn btn-primary btn-round">Change password</button>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div class="tab-pane " id="support">
                        <p>It’s one continuous form where hardware and software function in perfect unison, creating a new generation of phone that’s better by any measure.</p>
                        <p>Larger, yet dramatically thinner. More powerful, but remarkably power efficient. With a smooth metal surface that seamlessly meets the new Retina HD display. </p>
                      </div>
                      
                    </div>
                  </div>
                </div>
            </div>
        </div>
      </div>
    </div>


@endsection
