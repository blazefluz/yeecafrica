@extends('../../partials/app')

@section('sidebar')
@include('partials.memberSidebar')
@endsection

@section('navbar')
@include('partials.header')
@endsection

@section('content')

    <div class="row">
      <div class="col-md-4">
        <div class="card card-user">
          <div class="image">
            <img src= {{asset('assets/img/image_placeholder.jpg')}}
            alt="...">
          </div>
          <div class="card-body">
            <div class="author">
              <a href="#">
                <img class="avatar border-gray" src=
                  @if(isset($user->memberFormIimg))
                  {{asset('assets/img/image_placeholder.jpg')}}
                  @else
                  {{asset('/storage/'.$user->memberFormimg) }}
                  @endif
                alt="...">
                <h5 class="title" style="text-transform: capitalize">{{$user->fname}} {{$user->lname}}</h5>
              </a>
              <p class="description">
               
              </p>
            </div>
            <p class="description text-center">
              <small>{{$user->membership}}</small>
            </p>
          </div>
          <div class="card-footer">
            <hr>
            <div class="button-container">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-12 ">
                  <h5>{{$user->memberId}}
                    <br>
                    <small>Membership ID</small>
                  </h5>
                </div>
                
                
              </div>
            </div>
          </div>
        </div>
       
      </div>
      <div class="col-md-8">
        <div class="card card-user">
          <div class="card-header">
            <h5 class="card-title">Edit Profile</h5>
          </div>
          <div class="card-body">
            <form method="POST" action="{{url('update')}}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                          <label>First Name</label>
                          <input type="text" class="form-control @error('fname') is-invalid @enderror" name="fname" placeholder="First Name"  value="{{$user->fname}}">
                          @error('fname')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                    </div>
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                          <label>Middle Name</label>
                          <input type="text" class="form-control @error('mname') is-invalid @enderror" name="mname" placeholder="First Name"  value="{{$user->mname}}">
                          @error('mname')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                    </div>
                    <div class="col-md-4 pl-1">
                        <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control  @error('lname') is-invalid @enderror" name="lname" placeholder="Last Name" value="{{$user->lname}}">
                        @error('lname')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                </div>
              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group">
                    <label>Phone</label>
                    <input type="number" class="form-control  @error('phone') is-invalid @enderror" name="phone" placeholder="Phone" value="{{$user->phone}}">
                    @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control  @error('email') is-invalid @enderror" disabled value="{{$user->email}}" placeholder="Email">
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
              </div>

            
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Address</label>
                    <input type="text" class="form-control  @error('address') is-invalid @enderror" name="address" placeholder="Home Address" value="{{$user->address}}">
                    @error('address')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 pr-1">
                  <div class="form-group">
                    <label>City</label>
                    <input type="text" class="form-control  @error('city') is-invalid @enderror" name="city" placeholder="City" value="{{$user->city}}">
                    @error('city')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                      <label>State</label>
                      <input type="text" class="form-control  @error('state') is-invalid @enderror" name="state" placeholder="State" value="{{$user->state}}">
                      @error('state')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                    </div>
                </div>
                <div class="col-md-4 px-1">
                  <div class="form-group">
                    <label>Country</label>
                    <input type="text" class="form-control  @error('country') is-invalid @enderror" name="country" placeholder="Country" value="{{$user->country}}">
                    @error('country')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>

              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>About Me</label>
                    <textarea class="form-control textarea " name="about">{{$user->about}}</textarea>
                    
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="update ml-auto mr-auto">
                  <button type="submit" class="btn btn-primary btn-round">Update Profile</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>


@endsection
