@extends('../../partials/app')

@section('sidebar')
@include('partials/memberSidebar')
@endsection

@section('navbar')
@include('partials/header')
@endsection

@section('content')
@if ($user->status_id == 1 || $user->status_id != null)
  
@else
<div class="alert alert-warning">
  <span style="color:black; font-size: 18px" class="mr-auto">To continue please pay your membership dues</span> <a class="btn btn-default" href="{{url('/u/payment')}}">Pay Now</a>
</div>
@endif
<div class="row">
  
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center icon-warning">
                <i class="nc-icon nc-globe text-warning"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Programmes</p>
                <p class="card-title">2
                  <p>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center icon-warning">
                <i class="nc-icon nc-money-coins text-success"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Event</p>
                <p class="card-title">1
                  <p>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-12 col-md-12">
              <div class="numbers">
                <p class="card-category">YEEC ID</p>
                <small > {{$user->memberId}}
                </small>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center icon-warning">
                <i class="nc-icon nc-vector text-danger"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Membership</p>
                <small class="card-title">{{$user->membership}}
                </small>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center icon-warning">
                <i class="nc-icon nc-favourite-28 text-primary"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Status</p>
                <small class="card-title">
                  @if ($user->status_id == 1)
                    Active
                    @else
                    Disabled
                  @endif
                  
                </small>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-4">
      <div class="card ">
        <div class="card-header ">
          <h5 class="card-title">Events</h5>
          <p class="card-category">Latest events</p>
        </div>
        <div class="card-body ">

        </div>
        <div class="card-footer ">
          <hr>
          <div class="stats">
            <i class="fa fa-calendar"></i>No available event
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-8">
      <div class="card card-chart">
        <div class="card-header">
          <h5 class="card-title">Programmes</h5>
          <p class="card-category">Current Programmes</p>
        </div>
        <div class="card-body">
        </div>
        <div class="card-footer">
          <hr/>
          <div class="card-stats">
            <i class="fa fa-check"></i> No available Programmes
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
@section('script')
<script type="text/javascript">
  $(document).ready(function() {

      $('#datatables').DataTable({
          "pagingType": "full_numbers",
          "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          responsive: true,
          language: {
          search: "_INPUT_",
            searchPlaceholder: "Search records",
        }
      });


      var table = $('#datatables').DataTable();
       // Edit record
       table.on( 'click', '.edit', function () {
          $tr = $(this).closest('tr');

          var data = table.row($tr).data();
          alert( 'You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.' );
       } );

       // Delete a record
       table.on( 'click', '.remove', function (e) {
          $tr = $(this).closest('tr');
          table.row($tr).remove().draw();
          e.preventDefault();
       } );

      //Like record
      table.on( 'click', '.like', function () {
          alert('You clicked on Like button');
       });

  });
</script>
@endsection
