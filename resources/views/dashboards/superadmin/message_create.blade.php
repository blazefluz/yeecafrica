@extends('../../partials/app')

@section('sidebar')
    @include('partials.sidebar')
@endsection

@section('navbar')
    @include('partials.header')
@endsection

@section('content')
    <form action="{{route('message_create_store')}}" method="POST">
        @csrf
        <div class="row">
          <div class="col-md-12">
            <div class="card card-user">
              <div class="card-body">
                <div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Reciever</label>
                                <select name="receiver" class="form-control">
                                    <option value="3">Admin</option>
                                    <option value="5">All User</option>
                                    <option value="2">Paid User</option>
                                    <option value="1">Unpaid User</option>
                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Subject</label>
                                <input type="text" name="subject" class="form-control">
                            </div>
                        </div>
                    </div> --}}
                    <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>Create message</label>
                            <textarea  id="message1" class="form-control textarea "  name="message"></textarea>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="update ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary btn-round">send</button>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </form>
@endsection
@section('script')
<script>


    tinymce.init({
        selector: 'textarea#message1',
        height: 500,
        menubar: false,
        plugins: [
          'advlist autolink lists link image charmap print preview anchor',
          'searchreplace visualblocks code fullscreen',
          'insertdatetime media table paste code help wordcount'
        ],
        toolbar: 'undo redo | formatselect | ' +
        ' bold italic backcolor | alignleft aligncenter ' +
        ' alignright alignjustify | bullist numlist outdent indent |' +
        ' removeformat | help',
        content_css: [
          '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
          '//www.tiny.cloud/css/codepen.min.css'
        ]
      });
</script>

@endsection
