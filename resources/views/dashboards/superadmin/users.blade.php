@extends('../../partials/app')

@section('sidebar')
@include('partials.sidebar')
@endsection

@section('navbar')
@include('partials.header')
@endsection
@section('content')

    <div class="row">
     
      <div class="col-md-12">
        <div class="card card-user">
          <div class="card-header">
            
              <h5 class="card-title">Registered Members</h5>
              {{-- <a href="{{url('/payment/plan/create')}}" class="btn btn-success" >Update Plan</a> --}}
            
          </div>
          <div class="card-body">
            <div class="table-responsive"  id="table_dataa">
                {{-- {{dd($data)}} --}}
                {{-- @include('ajax/users_pagination') --}}
                
              <table id="datatables" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                <thead class="text-primary">
                  <tr><th class="text-center">
                    #
                  </th>
                  <th></th>
                  <th>
                    Name
                  </th>
                  <th>
                    Email
                  </th>
                  <th>
                    Phone
                  </th>
                  <th>
                    Address
                  </th>
                  <th>
                    Membership Code
                  </th>
                  <th>
                    Membership Type
                  </th>
                  <th>
                    Status
                  </th>
                  <th>
                    Date
                  </th>
                  <th>
                    Action
                  </th>
                
                </tr>
                </thead>
              <tbody>
                
              @if(count($data) > 0)
                @foreach ($data as $key => $row)
                {{-- {{dd($paymentPlans)}} --}}
                <tr>
                  <td class="text-center">
                  {{$key+=1}}
                  </td>
                  <td>
                      <div class="">
                          <div class="img-wrapper">
                            @if (!isset($row->memberFormimg))
                            <img src="{{asset('assets/img/image_placeholder.jpg')}}">
                            @else
                            <img src=" {{asset('/storage/'.$row->memberFormimg) }}">
                            @endif  
                          </div>
                      </div>
                  </td>
                  <td>
                    {{$row->fname}} {{$row->mname}} {{$row->lname}}
                  </td>
                  <td>
                    {{$row->email}}
                  </td>
                  <td>
                    {{$row->phone}}
                  </td>
                  <td>
                    {{$row->address}}
                  </td>
                  <td>
                    {{$row->memberId}}
                  </td>
                  <td>
                    {{$row->membership}}
                  </td>
                  <td>
                      @if ($row->member_verify == 2)
                      <span class="badge badge-pill badge-success">Paid</span>
                      @else
                      <span class="badge badge-pill badge-danger">Unpaid</span>
                      @endif
                  </td>
                  
                  <td class="text-right">
                    {{date('M d, Y', strtotime($row->created_at))}}
                  </td>
                  <td>
                    @if ($row->member_verify == 2)
                    <a class="btn btn-danger" href="{{url('/m/disable/'.$row->id)}}">Diactivate</a>
                    @else
                    <a class="btn btn-primary" href="{{url('/m/subscription/'.$row->id)}}">Activate</a>
                    @endif
                  </td>
                </tr> 
                @endforeach
                @endif
              </tbody>
              
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection
@section('script')
<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script>
    $(document).ready(function(){
    
     $(document).on('click', '.pagination a', function(event){
      event.preventDefault(); 
      var page = $(this).attr('href').split('page=')[1];
      fetch_data(page);
     });
    
     function fetch_data(page)
     {
      $.ajax({
       url:"/membership/users/data/?page="+page,
       success:function(data)
       {
        $('#table_data').html(data);
       }
      });
     }
     
    });
    </script>
  	<script type="text/javascript">
	    $(document).ready(function() {
      
	        $('#datatables').DataTable({
	            "pagingType": "full_numbers",
	            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
	            responsive: true,
	            language: {
                search: "_INPUT_",
                  searchPlaceholder: "Search records",
              },
              dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ]
              
	        });


	        var table = $('#datatables').DataTable();
	         // Edit record
	         table.on( 'click', '.edit', function () {
	            $tr = $(this).closest('tr');

	            var data = table.row($tr).data();
	            alert( 'You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.' );
	         } );

	         // Delete a record
	         table.on( 'click', '.remove', function (e) {
	            $tr = $(this).closest('tr');
	            table.row($tr).remove().draw();
	            e.preventDefault();
	         } );

	        //Like record
	        table.on( 'click', '.like', function () {
	            alert('You clicked on Like button');
	         });

	    });
	</script>
@endsection
