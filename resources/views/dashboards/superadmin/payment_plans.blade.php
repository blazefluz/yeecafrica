@extends('../../partials/app')

@section('sidebar')
@include('partials.sidebar')
@endsection

@section('navbar')
@include('partials.header')
@endsection
@section('content')

    <div class="row">
     
      <div class="col-md-12">
        <div class="card card-user">
          <div class="card-header">
            
              <h5 class="card-title">Payment Plans</h5>
              {{-- <a href="{{url('/payment/plan/create')}}" class="btn btn-success" >Update Plan</a> --}}
            
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class="text-primary">
                  <tr><th class="text-center">
                    #
                  </th>
                  <th>
                    Name
                  </th>
                  <th>
                    Code
                  </th>
                  <th>
                    Subscriptions
                  </th>
                  <th class="text-center">
                    Amount
                  </th>
                  <th class="text-right">
                    Interval
                  </th>
                  <th class="text-right">
                    Status
                  </th>
                  <th>
                    Created On
                  </th>
                </tr></thead>
                <tbody>
                  @php
                      $i=1;
                  @endphp
                  @foreach ($paymentPlans as $index => $plans)
                  {{-- {{dd($paymentPlans)}} --}}
                  <tr>
                    <td class="text-center">
                     {{$i++}}
                    </td>
                    <td>
                      {{$plans['name']}}
                    </td>
                    <td>
                      {{$plans['plan_code']}}
                    </td>
                    <td>
                      {{count($plans['subscriptions'])}}
                    </td>
                    <td class="text-center">
                      @php
                          
                      @endphp
                      {{ "₦ ".number_format(substr($plans['amount'], 0, -2))}}
                    </td>
                    <td class="text-right">
                      {{$plans['interval']}}
                    </td>
                    <td class="text-right">
                      {{'active'}}
                    </td>
                    <td class="text-right">
                      {{date('M d, Y', strtotime($plans['createdAt']))}}
                    </td>
                  
                  </tr> 
                  @endforeach
                 
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>


@endsection
