@extends('../../partials/app')

@section('sidebar')
@include('partials.sidebar')
@endsection

@section('navbar')
@include('partials.header')
@endsection
@section('content')

    <div class="row">
     
      <div class="col-md-12">
        <div class="card card-user">
          <div class="card-header">
            
              <a href="/message/create" class="btn btn-success push-right" >Create Message</a>
            
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class="text-primary">
                  <tr>
                      <th class="text-center">#</th>
                  <th>Messages</th>
                  <th class="text-right"> Created On</th>
                </tr>
                </thead>
                <tbody>
                  @php
                      $i=1;
                  @endphp
                  @foreach ($messages as $index => $message)
                  <tr>
                    <td class="text-center">
                     {{$i++}}
                    </td>
                    <td>
                      {!!$message->message!!}
                    </td>
                   
                    <td class="text-right">
                      {{date('M d, Y', strtotime($message->created_at))}}
                    </td>
                  </tr> 
                  @endforeach
                 
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>


@endsection
