
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="../assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
          Yeec portal
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <!-- CSS Files -->
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" />
        <link href="{{asset('assets/css/paper-dashboard.css?v=2.0.0')}}" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="{{asset('assets/demo/demo.css"')}}" rel="stylesheet" />
        <!-- Hotjar Tracking Code for www.app.yeecafrica.com -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:1720234,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-156902084-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-156902084-1');
        </script>
      </head>


        <body class="login-page"  >
                         <!-- Navbar -->
            <nav class="navbar navbar-expand-lg navbar-absolute fixed-top  " style="background-color:white">
                <div class="container">
                <div class="navbar-wrapper">
                
                    <a class="navbar-brand" href="javascript:;">
                        <div class="logo-image-big text-center">
                            <img width="150px"  src="{{asset('assets/img/logo-1.png')}}">
                        </div>
                    </a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navigation">
                    <ul class="navbar-nav">
                    
                    <li class="nav-item  ">
                        <a href="{{url('login')}}" class="btn  btn-default btn-round btn-block ">
                        <i class="nc-icon nc-tap-01"></i>
                        {{__('Sign In')}}
                        </a>
                    </li>
                    
                    
                    </ul>
                </div>
                </div>
            </nav>

            <div class="wrapper wrapper-full-page " style="margin-top: 0px" >
                <div class="full-page section-image" filter-color="black"
                style=" padding-top: 115px; padding-bottom: 105px;
                background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0.52), rgba(0, 0, 0, 0.73)),
                url('{{asset('/bg-2.jpg')}}'); background-size: cover"
                >
                    <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
                    <div class="content">
                    <div class="container card">
                        <div class="row ">
                        <div class="col-lg-6 col-md-6 mr-auto">
                            <div class="card-testimonial">
                                <div class="card-body ">
                                    <a href="javascript:;">
                                        <img class="img" src="/oil.jpg">
                                        </a>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-6 ml-auto">
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class=" card-login">
                                <div class="card-header ">
                                    <div class="card-header ">
                                        <a href="#" class="simple-text logo-normal">
                                            {{-- YEEC AFRICA --}}
                                            
                                        </a>
                                    <h6 class="header ">Join YEEC Africa</h6>
                                    </div>
                                </div>
                                <div class="card-body ">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            <i class="nc-icon nc-single-02"></i>
                                            </span>
                                        </div>
                                        <input id="fname" type="text" class="form-control @error('fname') is-invalid @enderror" name="fname" value="{{ old('fname') }}" placeholder="Firstname" required autocomplete="first name" autofocus>
                                            @error('fname')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            <i class="nc-icon nc-single-02"></i>
                                            </span>
                                        </div>
                                        <input id="lname" type="text" class="form-control @error('lname') is-invalid @enderror" name="lname" value="{{ old('lname') }}" placeholder="Lastname" required autocomplete="Last name" autofocus>
                                            @error('lname')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            <i class="nc-icon nc-mobile"></i>
                                            </span>
                                        </div>
                                        <input  type="number" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" placeholder="e.g +2340808000000" required  >
                                            @error('phone')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            <i class="nc-icon nc-email-85"></i>
                                            </span>
                                        </div>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email" required autocomplete="email" autofocus>
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            <i class="nc-icon nc-key-25"></i>
                                            </span>
                                        </div>
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            <i class="nc-icon nc-key-25"></i>
                                            </span>
                                        </div>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm password" required autocomplete="new-password">

                                    </div>
                                    <br/>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="" checked="" required>
                                        <span class="form-check-sign"></span>
                                          Accept terms and condition
                                        </label>
                                    </div>
                                    <label class="form-check-label">To be an active member of YEEC you must pay your dues to have your membership ID and access to all YEEC programs</label>
                                </div>
                                <div class="card-footer ">
                                    <button type="submit" class="btn btn-warning btn-round btn-block mb-3">
                                        {{ __('Sign Up') }}
                                    </button>
                                   
                                </div>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>
    </div>

<!--   Core JS Files   -->
<script src="{{asset('assets/js/core/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/core/popper.min.js')}}"></script>
<script src="{{asset('assets/js/core/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="{{asset('assets/js/plugins/chartjs.min.js')}}"></script>
<!--  Notifications Plugin    -->
<script src="{{asset('assets/js/plugins/bootstrap-notify.js')}}"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{asset('assets/js/paper-dashboard.min.js?v=2.0.0')}}" type="text/javascript"></script>
<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
<script src="{{asset('assets/demo/demo.js')}}"></script>
<script>
    $(document).ready(function() {
    // Javascript method's body can be found in assets/assets-for-demo/js/demo.js
    demo.initChartsPages();
    });
</script>
</body>

</html>
