

<div class="sidebar" data-color="white" data-active-color="danger">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
  -->
    <div class="logo">
      <a href="#" class="simple-text logo-mini">
        <div class="logo-image-small">
          <img src="../assets/img/logo-small.png">
        </div>
      </a>
      <a href="#" class="simple-text logo-normal">
        YEEC Africa
        <!-- <div class="logo-image-big">
          <img src="../assets/img/logo-big.png">
        </div> -->
      </a>
    </div>
    <div class="sidebar-wrapper ps-container ps-theme-default ps-active-x ps-active-y">
        <ul class="nav">
          <li class="active ">
            <a href="/superadmin">
              <i class="nc-icon nc-bank"></i>
              <p>Dashboard</p>
            </a>
          </li>
        
          <li>
            <a data-toggle="collapse" href="#pagesExamples" class="collapsed" aria-expanded="false">
              <i class="nc-icon nc-book-bookmark"></i>
              <p>
                Membership
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="pagesExamples" style="">
              <ul class="nav">
                <li>
                  <a href="{{url('/membership/users')}}">
                    <span class="sidebar-mini-icon">P</span>
                    <span class="sidebar-normal"> Users </span>
                  </a>
                </li>
                
              
              </ul>
            </div>
          </li>
          <li>
            <a data-toggle="collapse" href="#pagesExamples" class="collapsed" aria-expanded="false">
              <i class="nc-icon nc-book-bookmark"></i>
              <p>
                Payments
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="pagesExamples" style="">
              <ul class="nav">
                <li>
                  <a href="{{url('/payment/plans')}}">
                    <span class="sidebar-mini-icon">P</span>
                    <span class="sidebar-normal"> Plans </span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="sidebar-mini-icon">S</span>
                    <span class="sidebar-normal"> Subscription </span>
                  </a>
                </li>
              
              </ul>
            </div>
          </li>
          <li>
            <a href="./notifications.html">
              <i class="nc-icon nc-bell-55"></i>
              <p>Notifications</p>
            </a>
          </li>
          <li>
            <a href="{{url('/message')}}">
              <i class="nc-icon nc-mobile"></i>
              <p>Message</p>
            </a>
          </li>
          <li>
            <a href="./profile">
              <i class="nc-icon nc-single-02"></i>
              <p>Profile</p>
            </a>
          </li>
        </ul>
    </div>
  </div>
