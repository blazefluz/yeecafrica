

<div class="sidebar" data-color="white" data-active-color="danger">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
  -->
    <div class="logo">
      <a href="#" class="simple-text logo-mini">
        <div class="logo-image-small">
          <img src="{{asset('assets/img/logo-1.png')}}">
        </div>
      </a>
      <a href="#" class="simple-text logo-normal">
        {{-- YEEC AFRICA --}}
        <div class="logo-image-big">
          <img src="{{asset('assets/img/logo-1.png')}}">
        </div>
      </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
          <li class=" @if(isset($active_bar) && $active_bar == 'dashboard') active @endif ">
            <a href="{{url('./u/dashboard')}}">
              <i class="nc-icon nc-bank"></i>
              <p>Dashboard</p>
            </a>
          </li>

          <li class="@if(isset($active_bar) && $active_bar == 'notification') active @endif">
            <a href="{{url('./u/notification')}}">
              <i class="nc-icon nc-bell-55"></i>
              <p>Notifications</p>
            </a>
          </li>
          <li class="@if(isset($active_bar) && $active_bar == 'profile') active @endif">
            <a href="{{url('./u/profile')}}">
              <i class="nc-icon nc-single-02"></i>
              <p>Profile</p>
            </a>
          </li>
          <li class="@if(isset($active_bar) && $active_bar == 'program') active @endif">
            <a href="{{url('./u/programs')}}">
              <i class="nc-icon nc-paper"></i>
              <p>Programs</p>
            </a>
          </li>
          <li class="@if(isset($active_bar) && $active_bar == 'subscription') active @endif">
            <a href="{{url('/u/subscriptions')}}">
              <i class="nc-icon nc-cart-simple"></i>
              <p>Subscription</p>
            </a>
          </li>
          <li class="@if(isset($active_bar) && $active_bar == 'settings') active @endif">
            <a href="{{url('/u/settings')}}">
              <i class="nc-icon nc-settings-gear-65"></i>
              <p>Settings</p>
            </a>
          </li>
          <li class="active-pro" style="color:black;">
            <a href="{{url('/u/payment')}}" class="btn btn-warning btn-round "  style="color:white; background-color:red">
              <i class="nc-icon nc-spaceship"></i>
              Upgrade Membership
            </a>
          </li>
          <br>
        </ul>
    </div>
  </div>
