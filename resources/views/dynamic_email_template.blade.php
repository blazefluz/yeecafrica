


    @if($data['template_name'] == 'membership')

                <p>HI, {{ $data['name'] }}</p> 
                <p>Congratulations and welcome to YEEC Africa.</p>
                <p>
                At YEEC Africa, <br>
                
                We think big.<br>
                We Drive Progress <br>
                We go full speed ahead building a great and proud Africa. 
                
                Be rest assured that you stand to benefit from all the program of YEEC Africa that suits your category. <br>
                 <br>
                 <br>
                Your YEEC Membership Code is <strong>{{ $data['message'] }}</strong> <br>
                <br>
                
                For further enquiries, please call: 09034413665 or email: membership@yeecafrica.com<br>
                
                On behalf of our President and the executive management, we warmly welcome you to YEEC Africa.
                </p>
                <br>
                <br>
                <p>
                    MARY Ndefo (Mrs.) <br>
                    Director of Membership <br>
                    Mary.ndefo@yeecafrica.com <br>

                    YEEC Head Office-Nigeria <br>
                    House 27, Road 314, Trans Amadi Garden Estate, <br>
                    Odili Road, Port Harcourt <br>
                </p>
    @endif
    @if($data['template_name'] == 'oil-gas')
        <p>
            Hi, {{$data['name']}}<br>
            We hereby acknowledge receipt of your application for YEEC Oil and Gas
            Trainings. We appreciate your interest in career advancement through requisite
            capacity development.<br><br>
             
            Your application has been forwarded for eligibility screening.
            You will be duly informed regarding progress of procedure.<br><br>
             
            Having in mind our robust internal processes, be assured that you shall receive
            another email as a response to your application within 10 workdays.
            For further enquiries, send an email to programmes@yeecafrica.com or call one
            of our team members on +2348060418227<br>

            Sincerely, <br>

            Nglass, Eyisinbeinyim<br>
            Director of Programmes<br>
            nglass.eyisinbe@yeecafrica.com<br>
        </p>  
    @endif
    @if($data['template_name'] == 'scholarship')
        <p>
            Hi, {{$data['name']}}<br>
            We hereby acknowledge and appreciate your willingness to partner with YEEC
            AFRICA as a mentor.<br><br>
             
            Your application has been forwarded for review and you will be duly informed
            regarding progress of procedure.<br><br>
            
            Having in mind our robust internal processes, be assured that you shall receive
            another email as a response to your application within 10 workdays.
            For further enquiries, send an email to programmes@yeecafrica.com or call one
            of our team members on +2348060418227<br>
            Sincerely, <br>

            Nglass, Eyisinbeinyim<br>
            Director of Programmes<br>
            nglass.eyisinbe@yeecafrica.com<br>
        </p>
      
    @endif
    @if($data['template_name'] == 'seed-capital')
        <p>
            Hi, {{$data['name']}}<br>
            With this letter we hereby acknowledge receipt of your application for the YEEC
            Seed Capital Grant.
             <br><br>
            Your application has been forwarded to the screening committee. They shall
            proceed immediately with enquiries in an effort to consider your eligibility. You
            will be duly informed regarding progress of procedure.<br><br>
             
            Having in mind our robust internal processes, be assured that you shall receive
            another email as a response to your application within 10 workdays.
            For further enquiries, send an email to programmes@yeecafrica.com or call one
            of our team members on +2348060418227 <br><br>
            Sincerely, <br>

            Nglass, Eyisinbeinyim<br>
            Director of Programmes<br>
            nglass.eyisinbe@yeecafrica.com<br>
        </p>
      
    @endif
   
    @if($data['template_name'] == 'mentorship')
        <p>
            Hi,<br>
            We hereby acknowledge and appreciate your willingness to partner with YEEC
            AFRICA as a mentor.<br><br>
             
            Your application has been forwarded for review and you will be duly informed
            regarding progress of procedure.<br><br>
            
            Having in mind our robust internal processes, be assured that you shall receive
            another email as a response to your application within 10 workdays.
            For further enquiries, send an email to programmes@yeecafrica.com or call one
            of our team members on +2348060418227<br><br>

            Nglass, Eyisinbeinyim<br>
            Director of Programmes<br>
            nglass.eyisinbe@yeecafrica.com<br>
        </p>
      
    @endif
    @if($data['template_name'] == 'message')
        {!!$data['message']!!}
      
    @endif
   
