<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin', 'AdminController@index')->name('admin')->middleware('admin');


// Route::get('/superadmin', 'SuperAdminController@index')->name('superadmin')->middleware('superadmin');
Route::group(['middleware' => ['superadmin']], function () {
    Route::get('/superadmin', 'SuperAdminController@index')->name('superadmin');
    Route::get('/profile', 'SuperAdminController@profile')->name('profile');
    Route::get('/membership/users', 'SuperAdminController@users')->name('users');
    Route::get('membership/users/data', 'SuperAdminController@fetch_user_data')->name('users-data');
    Route::get('/payment/plans', 'SuperAdminController@paymentPlans')->name('plans');
    Route::get('/payment/plan/create', 'SuperAdminController@paymentPlanCreate')->name('plan_create');
   
    Route::get('/message', 'SuperAdminController@message')->name('message');
    Route::get('/message/create', 'SuperAdminController@message_create')->name('message_create');
    Route::post('/message/create/send', 'SuperAdminController@message_store')->name('message_create_store');
    Route::get('/m/subscription/{user_id}', 'PaymentController@manualpayment')->name('manualpayment');
    Route::get('/m/disable/{user_id}', 'PaymentController@deactivate')->name('deactivate');
});

Route::group(['middleware' => ['member']], function () {

    

    Route::get('/u/dashboard', 'MemberController@index')->name('member');
    Route::get('/u/profile', 'MemberController@profile')->name('profile');
    Route::Post('/u/update', 'MemberController@profileUpdate')->name('profileUpdate');
    Route::Post('/u/pay', 'PaymentController@redirectToGateway')->name('pay'); 
    Route::get('/u/subscription', 'PaymentController@handleGatewayCallback')->name('handlepay'); 
    Route::get('/u/payment', 'MemberController@payment')->name('payment'); 
    Route::get('/u/subscriptions', 'MemberController@subscriptions')->name('subscription'); 
    Route::get('/u/notification', 'MemberController@notification')->name('noti'); 
    Route::get('/u/programs', 'MemberController@programs')->name('programs'); 
    Route::get('/u/settings', 'MemberController@settings')->name('settings'); 

    //form
    Route::get('/u/create-step1', 'MemberController@createStep1');
    Route::post('/u/create-step1', 'MemberController@postCreateStep1');

    Route::get('/u/create-step2', 'MemberController@createStep2');
    Route::post('/u/create-step2', 'MemberController@postCreateStep2');
    Route::post('/u/remove-image', 'MemberController@removeImage');

    Route::get('/u/create-step3', 'MemberController@createStep3');
    Route::post('/u/create-step3', 'MemberController@postCreateStep3');
    
    Route::get('/u/create-step4', 'MemberController@createStep4');
    Route::post('/u/create-step4', 'MemberController@postCreateStep4');

    Route::get('/u/create-step5', 'MemberController@postCreateStep5');
    Route::post('/u/submission', 'MemberController@store');

    //program registration route
    Route::get('/u/seed-catipal/application', 'MemberController@createSeedCapitalForm');
    Route::post('/u/seed-catipal/application/store', 'MemberController@storeSeedCapitalForm');
    
    Route::get('/u/oil-gas/application', 'MemberController@createOilGasForm');
    Route::post('/u/oil-gas/application/store', 'MemberController@storeOilGasForm');

    Route::get('u/alert', 'MemberController@alert');
   

    //ajax
    Route::post('/lga', 'MemberController@lga');

});

Route::get('/manager', 'ManagerController@index')->name('manager')->middleware('manager');
Route::get('/staff', 'StaffController@index')->name('staff')->middleware('staff');

Route::get('storage/{filename}', function ($filename)
    {
        // return Image::make(storage_path('public/' . $filename))->response();
        return Storage::get('public/' . $filename);
    });