<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_form', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->nullable;
            $table->integer('program_id')->nullable;
            $table->string('bplan')->nullable;
            $table->string('baccount')->nullable;
            $table->string('bookkeepingskills')->nullable;
            $table->string('capitalneeds')->nullable;
            $table->string('identification_type')->nullable;
            $table->string('identification_image')->nullable;
            $table->string('descfundusage')->nullable;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_form');
    }
}
