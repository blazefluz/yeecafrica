<?php

namespace YEECAfrica;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Payments extends Model
{
    use Notifiable;

/**
 * The attributes that are mass assignable.
 *
 * @var array
 */
protected $fillable = [
    'userId', 'amount', 'authorization_code', 'customer_code', 'status',
    'payment_type','payment_plan', 'reference', 'expiresAt',
];
}
