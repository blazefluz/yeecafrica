<?php

namespace YEECAfrica;

use Illuminate\Database\Eloquent\Model;

class Payment_plan extends Model
{
    public $table = "payment_plan";

    protected $fillable = [
       'name', 'amount', 'code', 'interval',
    ];
}
