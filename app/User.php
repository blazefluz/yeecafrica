<?php

namespace YEECAfrica;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'fname', 'lname', 'role', 'email', 'password','memberFormimg',
            'mname', 'phone','address','city','state','lga','country','about',
            'dob', 'sex', 'raddress', 'religion', 'zipcode','occupation', 'organization',
            'degree', 'institution', 'matricno', 'yearg','aodegree', 'fieldinterest'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
