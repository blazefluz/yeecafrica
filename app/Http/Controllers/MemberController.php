<?php

namespace YEECAfrica\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use YEECAfrica\User;
use YEECAfrica\Payment_plan;
use YEECAfrica\Payments;
use Carbon;
use DB;
use Mail;
use YEECAfrica\Mail\SendMail;

class MemberController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
       
        $user = Auth::user();
        if($user->member_verify == 1 ){
            return redirect('/u/create-step5');
        }else if($user->member_verify == null){
            return redirect('/u/create-step1');
        }
        $active_bar = "dashboard";

        $request->session()->forget('memberForm');
        return view('dashboards.member.dashboard', compact('user', 'active_bar'));
    }
    public function alert(){
        $user = Auth::user();
        return view('dashboards.member.alert', compact('user'));
    }

    public function profile()
    {
        $user = Auth::user();
        if($user->member_verify == 1 ){
            return redirect('/u/create-step5');
        }elseif($user->member_verify == null){
            return redirect('/u/create-step1');
        }
        $active_bar = "profile";
        $user = Auth::user();
        return view('dashboards.member.profile', compact('user', 'active_bar'));
    }
    public function payment(){
        $plans = Payment_plan::get();
        
        $user = Auth::user();
        return view('dashboards.member.payment', compact('user', 'plans'));
    }

    public function notification(){
        $user = Auth::user();
        if($user->member_verify == 1 ){
            return redirect('/u/create-step5');
        }elseif($user->member_verify == null){
            return redirect('/u/create-step1');
        }
        $plans = Payment_plan::get();
        $active_bar = "notification";
        $user = Auth::user();
        return view('dashboards.member.notification', compact('user', 'plans','active_bar'));
    }
    public function programs(){
        $user = Auth::user();
        if($user->member_verify == 1 ){
            return redirect('/u/create-step5');
        }elseif($user->member_verify == null){
            return redirect('/u/create-step1');
        }
        $plans = Payment_plan::get();
        $active_bar = "program";
        
        
        return view('dashboards.member.programs', compact('user', 'plans', 'active_bar'));
    }
    public function subscriptions(){
        $user = Auth::user();
        if($user->member_verify == 1 ){
            return redirect('/u/create-step5');
        }elseif($user->member_verify == null){
            return redirect('/u/create-step1');
        }
        $active_bar = "subscription";
        $subscription = Payments::where('userId', $user->id)->get();
        return view('dashboards.member.subscription', compact('user','subscription', 'active_bar'));
    }
    public function settings(){
        $active_bar = "settings";
        $user = Auth::user();
        if($user->member_verify == 1 ){
            return redirect('/u/create-step5');
        }elseif($user->member_verify == null){
            return redirect('/u/create-step1');
        }
        return view('dashboards.member.settings', compact('user', 'active_bar'));
    }
    public function profileUpdate(Request $request)
    {
        // dd($request);
        $user = Auth::user();
        $data = $request->validate([
            'fname' => 'required|string|max:255',
            'lname' => 'required|string|max:255',
            // 'email' => 'required|string|email|unique:users|max:255',
            // 'phone' => 'required|string|max:15',
            'address' => 'required|string|max:225',
            'city' => 'required|string|max:225',
            'state' => 'required|string|max:225',
            'country' => 'required|string|max:225',
            'about' => 'string',
        ]);
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5048',
        ]);
  
        $imageName = time().'.'.$request->image->extension();  
   
        $request->image->move(public_path('images'), $imageName);

        User::where('id', $user->id)->update([
            'fname' => $data['fname'],
            'lname' => $data['lname'],
            // 'email' => $data['email'],
            'phone' => $data['phone'],
            'address' => $data['address'],
            'city' => $data['city'],
            'state' => $data['state'],
            'country' => $data['country'],
            'about' => $data['about'],
            'profileImage' => $imageName
        ]);
        return redirect('/u/profile');
    }



    // create form
    public function createStep1(Request $request)
    {
        $user = Auth::user();
        if($user->member_verify == 1 ){
            return redirect('/u/create-step5');
        }else if($user->member_verify == 2 ){
            return redirect('/');
        }
        $memberForm = $request->session()->get('memberForm');
        return view('dashboards.member.form.create-step1',compact('memberForm', 'user'));
    }

    public function postCreateStep1(Request $request)
    {
       

        $validatedData = $request->validate([
            'fname' => 'required',
            'mname' => 'max:255',
            'lname' => 'required',
            'phone' => 'required',
            'sex' => 'required',
            'zipcode' => 'max:255',
            'dob' => 'required',
            'religion' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'lga' => 'required',
            'country' => 'required',
            'about' => 'max:400',
        ]);

        

        if(empty($request->session()->get('memberForm'))){
            $memberForm = new User();
            $memberForm->fill($validatedData);
            $request->session()->put('memberForm', $memberForm);
            // dd($request->session()->get('memberForm'));
        }else{
            $memberForm = $request->session()->get('memberForm');
            $memberForm->fill($validatedData);
            $request->session(['memberForm' => $memberForm]);
            // dd($request->session()->get('memberForm'));
        }

        return redirect('/u/create-step2');

    }


     public function createStep2(Request $request)
    {

        $user = Auth::user();
        if($user->member_verify == 1 ){
            return redirect('/u/create-step5');
        }else if($user->member_verify == 2 ){
            return redirect('/');
        }
        $memberForm = $request->session()->get('memberForm');
        if(!isset($memberForm)){
            return redirect('/u/create-step1');
        }
        return view('dashboards.member.form.create-step2',compact('memberForm', 'user'));
    }


    public function postCreateStep2(Request $request)
    {
        $memberForm = $request->session()->get('memberForm');
        
        if(!isset($memberForm->memberFormImg)) {
            $request->validate([
                'memberFormimg' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);
           
            $fileName = "memberFormImage-" . time() . '.' . request()->memberFormimg->getClientOriginalExtension();

            $request->memberFormimg->storeAs('public', $fileName);

            $memberForm = $request->session()->get('memberForm');

            $memberForm->memberFormImg = $fileName;
            $request->session()->put('memberForm', $memberForm);
        }
        return redirect('/u/create-step3');

    }

    public function removeImage(Request $request)
    {
        $user = Auth::user();
        $memberForm = $request->session()->get('memberForm');
        $memberForm->memberFormImg = null;
        // return view('dashboards.member.form.create-step2',compact('memberForm', 'user'));
        return redirect('/u/create-step2');
    }

   
     public function createStep3(Request $request)
    {
        $user = Auth::user();
        if($user->member_verify == 1 ){
            return redirect('/u/create-step5');
        }else if($user->member_verify == 2 ){
            return redirect('/');
        }
        $memberForm = $request->session()->get('memberForm');
        if(!isset($memberForm->memberFormImg)){
            return redirect('/u/create-step2');
        }
        return view('dashboards.member.form.create-step3',compact('memberForm', 'user'));
    }

    public function postCreateStep3(Request $request)
    {
        

        $validatedData = $request->validate([
            'occupation' => 'required',
            'organization' => 'max:255',
            'degree' => 'required',
            'institution' => 'required',
            'matricno' => 'max:255',
            'fieldinterest' => 'required',
            'yearg' => 'required',
            'aodegree' => 'max:255',
            
        ]);

        // dd($validatedData);
        

        if(empty($request->session()->get('memberForm'))){
            $memberForm = new User();
            $memberForm->fill($validatedData);
            $request->session()->put('memberForm', $memberForm);
            // dd($request->session()->get('memberForm'));
        }else{
            $memberForm = $request->session()->get('memberForm');
            $memberForm->fill($validatedData);
            $request->session(['memberForm' => $memberForm]);
            // dd($request->session()->get('memberForm'));
        }


        
        return redirect('/u/create-step4');

    }
   
     public function createStep4(Request $request)
    {

        $user = Auth::user();
        if($user->member_verify == 1 ){
            return redirect('/u/create-step5');
        }else if($user->member_verify == 2 ){
            return redirect('/');
        }
        $memberForm = $request->session()->get('memberForm');
        if(!isset($memberForm->memberFormImg)){
            return redirect('/u/create-step3');
        }
        return view('dashboards.member.form.create-step4',compact('memberForm', 'user'));
    }

    public function postCreateStep4(Request $request)
    {
        
        
        

        if(empty($request->session()->get('memberForm'))){
            $memberForm = new User();
            $memberForm->fill($validatedData);
            $request->session()->put('memberForm', $memberForm);
            // dd($request->session()->get('memberForm'));
        }else{
            $memberForm = $request->session()->get('memberForm');
            $memberForm->fill($validatedData);
            $request->session(['memberForm' => $memberForm]);
            // dd($request->session()->get('memberForm'));
        }

        
        return redirect('/u/create-step4');

    }
    
    public function postCreateStep5(Request $request)
    {
        
        
        $plans = Payment_plan::get();
        $user = Auth::user();
       if($user->member_verify == null){
            return redirect('/u/create-step1');
        }else if($user->member_verify == 2 ){
            return redirect('/');
        }
        
        return view('dashboards.member.form.create-step5',compact( 'user', 'plans'));


    }

    public function store(Request $request)
    {

        $user = Auth::user();
        $memberForm = $request->session()->get('memberForm')->toArray();
        $data = collect($memberForm);
        $field = $data->put('member_verify', '1');
        $field = $data->pull('fieldinterest');
        // dd($data);
        DB::table('users')
            ->where('id', $user->id)
            ->update(json_decode($data, true));

        DB::table('users')
            ->where('id', $user->id)
            ->update(['fieldinterest' => json_encode($field) ]);

        return redirect('/u/create-step5');
    }

    public function lga(Request $request){
        $lga = $request->lga;
        return view('dashboards.member.ajax.ngstate', compact('lga'));
    }

    public function createSeedCapitalForm(Request $request){
        $user = Auth::user();
        
        return view('dashboards.member.form.seed-capital',compact( 'user'));
    }
    public function storeSeedCapitalForm(Request $request){

        $validatedData = $request->validate([
            'bplan' => 'required',
            'baccount' => 'required',
            'capitalneeds' => 'required',
            'bookkeepingskills' => 'required',
            'identification_type' => 'required',
            'identification_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'descfundusage' => 'required',
            
        ]);
  
        $identification_image = time().'.'.$request->identification_image->extension();  
   
        $request->identification_image->move(public_path('images'), $identification_image);

        
        $user = Auth::user();
        // $validatedData->put('user_id', $user->id);
        // dd($validatedData);
        DB::table('program_form')->insert(
        [
            'user_id' => $user->id,
            'program_id' => 1,
            'bplan' => $request->bplan,
            'baccount' => $request->baccount,
            'capitalneeds' => $request->capitalneeds,
            'bookkeepingskills' => $request->bookkeepingskills,
            'identification_type' => $request->identification_type,
            'identification_image' => $identification_image,
            'descfundusage' => $request->descfundusage,
        ]
        );
        $data = array(
            'template_name' => 'seed-capital',
            'name'      =>  $user->fname. ' '. $user->mname.' '. $user->lname,
            'message'   =>   'ThanK you for applying'
        );
        // dd($data);
        Mail::to($user->email)->send(new SendMail($data));
        return redirect('/u/alert')->with('success', 'Thanks for Applying!');
        
    }
    public function createOilGasForm(Request $request){
        $user = Auth::user();
        
        return view('dashboards.member.form.oil_gas',compact( 'user'));
    }
    public function storeOilGasForm(Request $request){

        $validatedData = $request->validate([
            'academicqual' => 'required',
            'pavalability' => 'required',
            'pbenefit' => 'required',
            'pskillsgain' => 'required',
            'admission_letter_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'acceptance_letter_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'degree_cert_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            
        ]);
  
        $admission_letter_image = time().'.'.$request->admission_letter_image->extension();  
        $request->admission_letter_image->move(public_path('images'), $admission_letter_image);
  
        $acceptance_letter_image = time().'.'.$request->acceptance_letter_image->extension();  
        $request->acceptance_letter_image->move(public_path('images'), $acceptance_letter_image);
      
        $degree_cert_image = time().'.'.$request->degree_cert_image->extension();  
        $request->degree_cert_image->move(public_path('images'), $degree_cert_image);

        
        $user = Auth::user();
        // $validatedData->put('user_id', $user->id);
        // dd($validatedData);
        DB::table('program_form')->insert(
        [
            'user_id' => $user->id,
            'program_id' => 2,
            'academicqual' => $request->bplan,
            'pavalability' => $request->baccount,
            'pbenefit' => $request->capitalneeds,
            'pskillsgain' => $request->bookkeepingskills,
            'admission_letter_image' => $admission_letter_image,
            'acceptance_letter_image' => $acceptance_letter_image,
            'degree_cert_image' => $degree_cert_image,
        ]
        );
        $data = array(
            'template_name' => 'oil-gas',
            'name'      =>  $user->fname,
            'message'   =>   ''
        );
        // dd($data);
        Mail::to($user->email)->send(new SendMail($data));
        return redirect('/u/alert')->with('success', 'Thanks for Applying!');
        
    }
}
