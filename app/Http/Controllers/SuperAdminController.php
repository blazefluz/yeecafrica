<?php

namespace YEECAfrica\Http\Controllers;

use Illuminate\Foundation\Auth\User;

use Auth;
use DB;
use Illuminate\Http\Request;
use YEECAfrica\Payment_plan;
use YEECAfrica\Payments;
use Mail;
use YEECAfrica\Mail\SendMail;

use Paystack;

class SuperAdminController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userCount = DB::table('users')->where('role','=', 5)->count();
        $userCount = DB::table('users')->where('role','=', 5)->count();
        $user = Auth::user();
        return view('dashboards.superadmin.dashboard',compact('user', 'userCount'));
    }

    public function profile()
    {
        $user = Auth::user();
        return view('dashboards.superadmin.profile', compact('user'));
    }

    public function message()
    {
        $messages = DB::table('message')->paginate(15);
        $user = Auth::user();
        return view('dashboards.superadmin.message', compact('user', 'messages'));
    }
    public function message_create(Request $request)
    {
        $user = Auth::user();
        return view('dashboards.superadmin.message_create', compact('user'));
    }
    public function message_store(Request $request)
    {
        $validatedData = $request->validate([
            'receiver' => 'required',
            'message' => 'required',
        ]);
        DB::table('message')->insert([
            'receiver' => $request->receiver,
            'message' => $request->message,
        ]);
        
       
        // dd($data);
        $recieverData =  DB::table('users')->where('role',  $request->receiver)->orWhere('member_verify',  $request->receiver)->get();
            foreach($recieverData as $key => $ut){
            $ua = [];
            $ua['email'] = $ut->email;
            $ua['name'] = $ut->fname;
            $reciever[$key] = (object)$ua;
            }

            $data = array(
            'template_name' => 'message',
            'name'      =>  '',
            'message'   =>   $request->message
        );

        Mail::bcc($reciever)->queue(new SendMail($data));
        // Mail::send(new SendMail($data) function ($message) use ($reciever)
        // {
        //     $message->from('myemail@gmail.com','Hello Laravel');
        //     $message->to($reciever)->subject('Welcome to Laravel Mailables');
        // });
            return redirect('/message');
      
    }

    public function users(){
        $data = DB::table('users')->where('role','=', 5)->get();
        $user = Auth::user();
        return view('dashboards.superadmin.users', compact('user', 'data'));
    }
    function fetch_user_data(Request $request){
        $user = Auth::user();
        if($request->ajax())
        {
        $data = DB::table('users')->where('role','=', 5)->paginate(15);
        return view('ajax.users_pagination', compact('user', 'data'))->render();
        }
    }
    public function paymentPlans(){
        try{
            $paymentPlans = Paystack::getAllPlans();
           
          
        }catch(Exception $e){
            echo "unable to load data" .$e->getMessage();
        }
     
        $user = Auth::user();
        return view('dashboards.superadmin.payment_plans', compact('user', 'paymentPlans'));
    }

    public function paymentPlanCreate(){
        $paymentPlans = Paystack::getAllPlans();
       
       foreach($paymentPlans as $plans)
        if(Payment_plan::where('code', $plans['plan_code'])->count() == 1){
            Payment_plan::where('code', $plans['plan_code'])->update([
                'name' => $plans['name'],
                'amount' => $plans['amount'],
                'code' => $plans['plan_code'],
                'interval' => $plans['interval'],
            ]);
            $user = Auth::user();
            return view('dashboards.superadmin.payment_plans', compact('user', 'paymentPlans'));
  
        }else{
            Payment_plan::create([
                'name' => $plans['name'],
                'amount' => $plans['amount'],
                'code' => $plans['plan_code'],
                'interval' => $plans['interval'],
            ]);
        }

        $user = Auth::user();
        return view('dashboards.superadmin.payment_plans', compact('user', 'paymentPlans'));
    }
}
