<?php

namespace YEECAfrica\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use YEECAfrica\User;
use YEECAfrica\Payments;
use YEECAfrica\Http\Requests;
use YEECAfrica\Http\Controllers\Controller;
use Paystack;
use Mail;
use YEECAfrica\Mail\SendMail;

class PaymentController extends Controller
{

    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway()
    {
        return Paystack::getAuthorizationUrl()->redirectNow();
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function manualpayment($user_id){
        $user = User::where(['id' => $user_id])->first();
        $uniqueCode = getToken(8);
        Payments::create([
                'userId' => $user_id,
                'amount' => '0000',
                'status' => 'success',
                'payment_type' => 'manual',
                'payment_plan' => 'Graduate',
         
              
            ]);
            User::where('id', $user_id)->update([
                'memberId' => $uniqueCode,
                'status_id' => 1,
                'member_verify' => 2,
                'membership' => 'Graduate',
            ]);

            
        $data = array(
            'template_name' => 'membership',
            'name'      =>  $user->fname,
            'message'   =>   $uniqueCode,
        );
        // dd($data);
        Mail::to($user->email)->send(new SendMail($data));
        return back()->with('success', 'Thanks for Applying!');
    }
   
    public function deactivate($user_id){
     
            User::where('id', $user_id)->update([
                'memberId' => '',
                'status_id' => 2,
                'member_verify' => 1,
                'membership' => '',
            ]);
            
        return back()->with('success', 'Thanks for Applying!');
    }

    public function handleGatewayCallback()
    {
        $paymentDetails = Paystack::getPaymentData();

        // dd($paymentDetails['data']['reference']);
        $user = Auth::user();
       $authorize = $paymentDetails['data']['authorization']['authorization_code'];
        if(Payments::where('authorization_code', $authorize)->count() == 1){
            return redirect('/u/payment');
        }
        else{
            Payments::create([
                'userId' => $user->id,
                'amount' => $paymentDetails['data']['amount'],
                'authorization_code' => $paymentDetails['data']['authorization']['authorization_code'],
                'customer_code' => $paymentDetails['data']['customer']['customer_code'],
                'status' => $paymentDetails['data']['status'],
                'payment_type' => $paymentDetails['data']['plan_object']['description'],
                'payment_plan' => $paymentDetails['data']['plan_object']['name'],
                'reference' => $paymentDetails['data']['reference'],
              
            ]);
            $uniqueCode = getToken(8);
            if(empty($user->memberId)){
                User::where('id', $user->id)->update([
                    'memberId' =>  $uniqueCode,
                    'status_id' => 1,
                    'member_verify' => 2,
                    'membership' => $paymentDetails['data']['plan_object']['name'],
                ]);
                $data = array(
                    'template_name' => 'membership',
                    'name'      =>  $user->fname,
                    'message'   =>   $uniqueCode
                );
            }else{
                User::where('id', $user->id)->update([
                    'membership' => $paymentDetails['data']['plan_object']['name'],
                ]);
                $data = array(
                    'template_name' => 'membership',
                    'name'      =>  $user->fname,
                    'message'   =>   $user->memberId
                );
            }
        };

       
        // dd($data);
        Mail::to($user->email)->send(new SendMail($data));
        return redirect('/u/programs')->with('success', 'Thanks for Applying!');
        
       
        // return redirect('/u/profile');
        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want
    }
}